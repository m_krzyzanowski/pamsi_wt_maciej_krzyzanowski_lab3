#include <iostream>
#include <windows.h>
#include <conio.h>
#include <stack>

using namespace std;

template <typename Object>
void dodaj_element(stack <Object> & s1)
{
    Object elem;
    cout << "Podaj element: ";
    while(!(cin >> elem))
    {
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }
    s1.push(elem);
}

template <typename Object>
void usun_element(stack <Object> & s1)
{
    if(s1.empty()) cout << "Stos jest pusty.";
    else s1.pop();
}

template <typename Object>
void usun_wszystkie(stack <Object> & s1)
{
    for(int i = s1.size(); i>0; i--)
        s1.pop();
}

template <typename Object>
void wyswietl(stack <Object> s1)
{
    stack <Object> nowy;
    nowy = s1;
    cout << "---------- STOS ----------" << endl;
    for(int i = nowy.size(); i>0; i--)
    {
        cout << nowy.top() << " ";
        nowy.pop();
    }
    cout << endl << "--------------------------" << endl;
}



int main()
{
    stack <int> s1; //alokacja stosu typu int

    	char option;
	do{
        Sleep(200);
        system("cls");
		wyswietl(s1);
		cout << "==== MENU GLOWNE ===" << endl;
		cout << "1.Dodaj element." << endl;
		cout << "2.Usun element." << endl;
		cout << "3.Usun wszystkie elementy." << endl;
		cout << "0.Wyjscie" << endl;
		cout << "Podaj opcje:";
		option = getche();
		cout << endl;

		switch (option){
		case '1':
			dodaj_element(s1);
			break;

		case '2':
			usun_element(s1);
			break;

		case '3':
			usun_wszystkie(s1);
			break;
		}

	} while (option != '0');


	return 0;
}
