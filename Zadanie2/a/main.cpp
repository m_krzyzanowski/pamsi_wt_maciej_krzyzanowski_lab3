#include <iostream>
#include <windows.h>
#include <conio.h>
#include "kol_tab_inkr.h"

using namespace std;

template <typename Object>
void dodaj_element(Kolejka <Object> & k1)
{
    Object elem;
    cout << "Podaj element: ";
    while(!(cin >> elem))
    {
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }
    k1.enqueue(elem);
}

template <typename Object>
void usun_element(Kolejka <Object> & k1)
{
    k1.dequeue();
}

template <typename Object>
void usun_wszystkie(Kolejka <Object> & k1)
{
    for(int i=0; i=k1.size(); i++)
        k1.dequeue();
}

template <typename Object>
void wyswietl(Kolejka <Object> k1)
{
    cout << "---------- KOLEJKA ----------" << endl;
        k1.wyswietl();
    cout << endl << "-----------------------------" << endl;
}



int main()
{
    Kolejka <int> k1; //alokacja stosu typu int

    	char option;
	do{
        Sleep(200);
        system("cls");
		wyswietl(k1);
		cout << "==== MENU GLOWNE ===" << endl;
		cout << "1.Dodaj element." << endl;
		cout << "2.Usun element." << endl;
		cout << "3.Usun wszystkie elementy." << endl;
		cout << "0.Wyjscie" << endl;
		cout << "Podaj opcje:";
		option = getche();
		cout << endl;

		switch (option){
		case '1':
			dodaj_element(k1);
			break;

		case '2':
			usun_element(k1);
			break;

		case '3':
			usun_wszystkie(k1);
			break;
		}

	} while (option != '0');


	return 0;
}

