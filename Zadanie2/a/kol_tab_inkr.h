#include <iostream>

using namespace std;

template <typename Object>
class Kolejka
{
private:
    int MAX_SIZE; //pojemnosc stosu
    int ogon;
    Object *K; //tablica stosu

public:
    Kolejka(); //konstruktor kolejki
    ~Kolejka(); //destruktor kolejki

    void enqueue(const Object & nowy); //dodaje element na koncu kolejki
    Object dequeue(); //usuwa element i zwraca element z poczatku kolejki
    Object & front() const; // zwraca element na przodzi kolejki bez usuwania go
    int size() const; //zwraca ilosc przechowywanych elementow
    bool isEmpty() const; // sprawdza czy kolejka jest pusta
    void wyswietl();
};


//---------------------Metody--------------------

template <typename Object>
Kolejka<Object>::Kolejka()
{
    MAX_SIZE = 1;
    K = new Object[MAX_SIZE];
    ogon = 0;
}

template <typename Object>
Kolejka<Object>::~Kolejka()
{
    delete [] K;
}

template <typename Object>
void Kolejka<Object>::enqueue(const Object & nowy)
{
    if(ogon == MAX_SIZE)
    {
        MAX_SIZE*=2;
        Object *nowyK = new Object[MAX_SIZE];

        for(int i=0; i<=ogon; i++)
            nowyK[i] = K[i];

        delete[] K;
        *K = *nowyK;
        K[ogon] = nowy;
        ogon++;
    }
    else
    {
        K[ogon] = nowy;
        ogon++;
    }
}

template <typename Object>
Object Kolejka<Object>::dequeue()
{
    if(isEmpty()) cerr << "Kolejka jest PUSTA" << endl;
    else
    {
        Object usuwany = *K;
        for(int i=0; i<size()-1; i++)
            K[i] = K[i+1];
        ogon--;
        return usuwany;
    }
}

template <typename Object>
Object & Kolejka<Object>::front() const
{
    if(isEmpty()) cerr << "Kolejka jest PUSTA" << endl;
    else return *K;
}

template <typename Object>
int Kolejka<Object>::size() const
{
    return ogon;
}

template <typename Object>
bool Kolejka<Object>::isEmpty() const
{
    return (ogon==0);
}

template <typename Object>
void Kolejka<Object>::wyswietl()
{
    for(int i=0; i<size(); i++)
        cout << K[i] << " ";
}
